'''
Contexto de ejecución (CE)
    CE Nivel 1  (CEN 1)
    CE Nivel n  (CEN 2)
'''
def function_1(arg_1, func_1): # CEN 1
    def function_2(an_argument):  # CEN 2
        return an_argument * 2 # CEN 3

    value = function_2(arg_1) # CEN 2
    return func_1(value) # CEN 2

arg_1 = 1 # CEN 1

def any_function(any_arg): # CEN 1
    return any_arg + 5 # CEN 2

resul = function_1(arg_1, any_function) # CEN 1
print(resul) # CEN 1

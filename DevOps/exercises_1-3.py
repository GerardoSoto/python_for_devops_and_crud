def list_comprehension(phrase):
    phrase_capitalized = ''
    phrase_capitalized = [letter.upper() for letter in phrase]
    return phrase_capitalized

def run():
    phrase = 'smogrether'
    print('Phrase original: {}'.format(phrase))
    phrase_capitalized = list_comprehension(phrase)
    print('Phrase capitalized: {}'.format(phrase_capitalized))

if __name__ == '__main__':
    run()

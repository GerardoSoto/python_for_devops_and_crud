def name_of_user(name):
    '''This method print the name of user with
    the argument name'''
    print('The name of user is: {}'.format(name))

def run():
    '''First method to start program'''
    name = input('Input your name: ')
    name_of_user(name)


if __name__ == '__main__':
    run()

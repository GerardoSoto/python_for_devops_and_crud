#!/bin/bash

try() {
    module=$1
    echo "f() try - module = $module"
    message=`python3.5 -c "
exec('''
print('module to search: ')
try:
    import ${module} as module 
    print(module.__file__)
    print(${module})
except Exception as e:
    print(e)
''')"`
    echo $message
}
echo "Start"
echo "Does the module: $1 exist?"
try $1
echo "End."


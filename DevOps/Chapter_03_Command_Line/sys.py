import sys


print('Architecture: {}'.format(sys.byteorder))
print('Size of Python Object(1): {}'.format(sys.getsizeof(1)))
print('Perform behaviors (OS): {}'.format(sys.platform))

if sys.version_info.major < 3:
    print('You need to update your Python version. {}.{}'.format(sys.version_info.major, sys.version_info.minor))
elif sys.version_info.minor < 7:
    print('You are not running the latest Python version. {}.{}'.format(sys.version_info.major, sys.version_info.minor))
else:
    print('All is good. Python version: {}.{}'.format(sys.version_info.major, sys.version_info.minor))

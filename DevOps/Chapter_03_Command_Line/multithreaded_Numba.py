''' Running True Multithreaded Python using Numba'''

#from decorators import *
import timeit
from numba import jit

@timeit
@numba.jit(parallel = True)
def add_sum_threaded(rea):
    '''Use all the cores'''

    x,_ = rea.shape
    total = 0
    for _ in numba.prange(x):
        total += rea.sum()
        print(total)


@timeit
#@timing
def add_sum(rea):
    '''Tradittional for loop'''

    x,_ = rea.shape
    total = 0
    for _ in numba.prange(x):
        total += rea.sum()
        print(total)


@cli.command()
@click.option('--threads/--no-jit', default = False)
def thread_test(threads):
    rea = real_estate_array()
    if threads:
        click.echo(click.style('Running with multicore threads', fg = 'green'))
        add_sum_threaded(rea)
    else:
        click.echo(click.style('Running NO threads', fg = 'red'))
        add_sum(rea)


def name_in_upper(name):
    '''This method recive the name of the
    user and print the name in Capitalize'''
    print('The name of user is: {}'.format(name.upper()))


def run():
    '''First method to start the program'''
    name = str(input('Input your name: '))
    name_in_upper(name)


if __name__ == '__main__':
    run()

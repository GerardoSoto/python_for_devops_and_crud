from cryptography.fernet import Fernet

key = Fernet.generate_key()

print('The key generate: {}'.format(key))

f = Fernet(key)

message = b"Secrets go here"
message_2 = b"Secong secret"

print(message)
print(message_2)

encrypted_message_1 = f.encrypt(message)
encrypted_message_2 = f.encrypt(message_2)

print('Messages encrypted:***************************************')
print(encrypted_message_1)
print(encrypted_message_2)

print('Messages Desencrypted:-----------------------------------')
encrypted_message_1 = f.decrypt(encrypted_message_1)
print(encrypted_message_1)
encrypted_message_2 = f.decrypt(encrypted_message_2)
print(encrypted_message_2)

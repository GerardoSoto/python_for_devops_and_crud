def spell_word_1(word_1):
    for letter in word_1:
        yield letter

def spell_word_2(word_2):
    for letter in word_2:
        yield letter

def run():
    word_1 = 'new'
    word_2 = 'old'
    spell_1 = spell_word_1(word_1)
    spell_2 = spell_word_2(word_2)
    for letter in word_1:
        print('{}'.format(next(spell_1)))
        print('{}'.format(next(spell_2)))


if __name__ == '__main__':
    run()

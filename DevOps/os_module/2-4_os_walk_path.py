#!/usr/bin/env python3


import fire
import os
import time


def walk_path(parent_path):
    print('Start:   Checking: {}'.format(parent_path))
    print('os.walk= {}'.format(os.walk(parent_path)))
    for parent_path, directories, files in os.walk(parent_path, topdown=False):
        print('Checking: {}'.format(parent_path))
        for file_name in files:
            file_path = os.path.join(parent_path, file_name)
            last_access = os.path.getatime(file_path)
#            last_access = os.path.getctime(file_path)
#            last_access = os.path.getmtime(file_path)
            last_access_local_time = time.ctime(last_access)
            size = os.path.getsize(file_path)
            print('(dir)File: {}'.format(file_path))
            print('File: {}'.format(file_name))
            print('Last access: {}'.format(last_access))
            print('Last access (Local time): {}'.format(last_access_local_time))
            print('Size: {}'.format(size))
            print('*' * 15)
        #elif os.path.isdir(child_path):
         #   walk_path(child_path)


if __name__ == '__main__':
    fire.Fire()

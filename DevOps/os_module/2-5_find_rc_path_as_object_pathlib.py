import os
import pathlib

def find_rc(rc_name = ".examplerc"):
    # Check for ENV variable:
    var_name = "EXAMPLERC_DIR"
    # As of this writing {pathlib} does not expand environment variables. Instead you grab the value of the variable from os.environ:
    example_dir = os.environ.get(var_name)
    # Ckeck whether the environment variable exists in the current environment:
    if example_dir:
        # This creates a pathlib.Path object appropriate for the currently running operate systen:
        dir_path = pathlib.Path(example_dir)
        # You can construct new pathlib.Path objects by following a parent path with forward slashes and strings:
        config_path = dir_path / rc_name
        print('Checking Env: {}'.format(config_path))
        # The pathlib.Path ob ject itself has an exists method. USING A METHOD OOP
        if config_path.exists():
            # Call as_posxif to return the path as a string. Depending of your use case, tou can return the pathlib.Path object itself:
            return config_path.as_posix()

    # Checking the current working directory
    # The class method pathlib.Path.cwd return a pathlib.Path object for the current working directory. This oject is used immediately here to create the config_path by joining it whit the string rc_name:
    config_path = pathlib.Path.cwd() / rc_name
    print('Checking cwd: {}'.format(config_path))
    if config_path.exists():
        return config_path.as_posix()

    # Check user home directory
    # The class method pathlib.Path.home() returns a pathlib.Path object for the current user's home directory:
    ### home_dir = os.path.expanduser("~/")
    config_path = pathlib.Path.home() / rc_name
    print('Checking home: {}'.format(config_path))
    if os.path.exists(config_path):
        return config_path.as_posix()

    # Check directory of this file
    # Create a pathlib.Path object using the relative path stored in [thisfile] and then call its resolve method to get the absolute path:
    file_path = pathlib.Path(__file__).resolve()
    # This returns a parent pathlib.Path Object directly from the object itself:
    parent_path = file_path.parent## Si esta bien, sin parentesis ().
    config_path = parent_path / rc_name
    print('Checking: {}'.format(config_path))
    if os.path.exists(config_path):
        return config_path.as_posix()

    print('File {} has not been founf'.format(rc_name))

if __name__ == '__main__':
    find = find_rc()
    if find:
        print('File Found.')


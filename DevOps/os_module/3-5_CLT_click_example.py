#!/usr/bin/env python

'''
Command-line tool using argparse
'''

import click


@click.group()# Create a top-level group under which other groups and commands will reside
def cli():# create a function to act as the top-level group. The click.group method transforms the function into a group
    pass

@click.group(help = 'Ship related commands')# Create a group to hold the ships commands
def ships():
    pass


cli.add_command(ships)# Add the ships group as a command to the top-level group. Note that the cli function is now a group with an add_command method.


@ships.command(help = 'Sail a ship')# Add a command to the ships group. Notice that ships.command is used instead of click.command.
def sail():
    ship_name = 'Your ship'
    print('{} is setting sail'.format(ship_name))


@ships.command(help = 'List all of the ships')
def list_ships():
    ships = ['Jonas', 'Yankee Clipper', 'Lary']
    print("{} is setting sail")


@cli.command(help = 'Talk to a sailor')# Add a command to the cli group
@click.option('--greeting', default='Ahoy there', help = 'Greeting for sailor')
@click.argument('name')
def sailors(greeting, name):
    message = "{} {}".format(greeting, name)
    print(message)

    
if __name__ == '__main__':
    cli()# Call to the top-level group 


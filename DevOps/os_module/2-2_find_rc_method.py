import os

def find_rc(rc_name = ".examplerc"):
    # Check for ENV variable
    var_name = "EXAMPLERC_DIR"
# Ckeck whether the environment variable exists in the current environment.
    if var_name in os.environ:
        # Use join to construct the path with the environment variable name.
        # This will look something like $EXAMPLE_DIR/.examplerc
        var_path = os.path.join("${}".format(var_name),rc_name)
        # Expand the environment variable to insert its value into the path.
        config_path = os.path.expandvars(var_path)
        print('Checking Env: {}'.format(config_path))
        # Check to see if the file exists
        if os.path.exists(config_path):
            return config_path

    # Checking the current working directory
    # Construct the path with the current working dirctory
    config_path = os.path.join(os.getcwd(), rc_name)
    print('Checking cwd: {}'.format(config_path))
    if os.path.exists(config_path):
        return config_path

    # Check user home directory
    # Use the expanderuser function to get the path of the user's home directory
    home_dir = os.path.expanduser("~/")
    config_path = os.path.join(home_dir,rc_name)
    print('Checking home: {}'.format(config_path))
    if os.path.exists(config_path):
        return config_path

    # Check directory of this file
    # Expand the relative path stored in FILEto an absolute path
    file_path = os.path.abspath(__file__)
    # Use dirname to get the path to the directory holding the current file
    parent_path = os.path.dirname(file_path)
    config_path = os.path.join(parent_path, rc_name)
    print('Checking: {}'.format(config_path))
    if os.path.exists(config_path):
        return config_path

    print('File {} has not been founf'.format(rc_name))

if __name__ == '__main__':
    find = find_rc()
    if find:
        print('File Found.')


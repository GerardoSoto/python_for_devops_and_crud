#!/usr/bin/python3
import os

current_dir = os.getcwd()
print('Current directory: {}'.format(current_dir))

current_dir_split = os.path.split(current_dir)
print('Current directory splited: {}'.format(current_dir_split))

dir_name = os.path.dirname(current_dir)
print('Current path: {}'.format(dir_name))

dir_basename = os.path.basename(current_dir)
print('Directory basename: {}'.format(dir_basename))

print('Walk up to directory tree:')
while os.path.basename(current_dir):
    current_dir = os.path.dirname(current_dir)
    print(current_dir)


#!/usr/bin/env python3


import fire
import os


def walk_path(parent_path):
    print('Checking: {}'.format(parent_path))
    childs = os.listdir(parent_path)

    for child in childs:
        child_path = os.path.join(parent_path, child)
        if os.path.isfile(child_path):
            last_access = os.path.gettime(child_path)
            size = os.path.getsize(child_path)
            print('(dir)File: {}'.format(child_path))
            print('File: {}'.format(child))
            print('Last access: {}'.format(last_access))
            print('Size: {}'.format(size))
            print('*' * 15)
        elif os.path.isdir(child_path):
            walk_path(child_path)


if __name__ == '__main__':
    fire.Fire()

import sys, csv, os

clients = []

CLIENT_TABLE = '.clients.csv'
CLIENT_SCHEMA = ['name', 'company', 'email', 'position']

def _initialize_clients_from_storage():
    with open(CLIENT_TABLE, mode='r') as my_file:
        reader = csv.DictReader(my_file, fieldnames = CLIENT_SCHEMA)
        for row in reader:
            clients.append(row)


def _save_clients_to_storage():
    tmp_table_name = '{}.tmp'.format(CLIENT_TABLE)
    with open(tmp_table_name, mode='w') as my_file:
        writer = csv.DictWriter(my_file, fieldnames = CLIENT_SCHEMA)
        writer.writerows(clients)

        os.remove(CLIENT_TABLE)
        os.rename(tmp_table_name, CLIENT_TABLE)


def create_client(client):
    global clients
    if client not in clients:
        clients.append(client)
    else:
        print('Client already exist in the client\'s list.')

def list_clients_v0_1():
    print(clients)

def list_clients():
    for idx, client in enumerate(clients):
        print('{uid}: {name} | {company} | {email} | {position}'.format(
            uid=idx,
            name=client['name'],
            company=client['company'],
            email=client['email'],
            position=client['position']))


def update_client(client_name,update_client_name):
    global clients
    for idx, client in enumerate(clients):
        # Busqueda del cliente en la lista:
        if client['name'] == client_name:
            client['name'] = update_client_name
    else:
        #print(f'Client: {client_name} is not in clients list.') py 3.6+
        print('Client: {0} is not in clients list'.format(client_name))


def delete_client(client_name):
    global clients
    for idx, client in enumerate(clients):
        if client['name'] == client_name:
            clients.pop(idx)
    else:
        print('Client: {0} is not in clients list'.format(client_name))


def read_client():
    #type(clients)
    found = False
    option_search = input('''Search options:
    a) Name
    b) Company
    c) e-mail
    d) Position  :::''')
    option_search = option_search.upper()
    client_name = _get_client_name()
    if option_search == 'A':
        for idx, client in enumerate(clients):
            if client['name'] == client_name:
                print('{}: {}'.format(idx,client))
                found = True
                break 
            found = False
        if found:
            print('The client {} is in the client\'s list'.format(client_name))
        else:
            print('The client {} isn\'t in client\'s list'.format(client_name))
    elif option_search == 'B':
        for idx, client in enumerate(clients):
            if client['company'] == client_name:
                print('{}: {}'.format(idx,client))
                found = True
                #break
            #found = False
        if not found:
            print('There aren\'t clients working in the compañy: {}'.format(client_name))
    elif option_search == 'C':
        for idx, client in enumerate(clients):
            if client['email'] == client_name:
                print('{}: {}'.format(idx,client))
                found = True
                break
            found = False
        if not found:
            print('There aren\'t client with e-mail: {}'.format(client_name))
    elif option_search == 'D':
        for idx, client in enumerate(clients):
            if client['position'] == client_name:
                print('{}: {}'.format(idx,client))
                found = True
                #break
            #found = False
        if not found:
            print('There aren\'t clients with the position: {}'.format(client_name))
    else:
        print('Invalid command.')


def _print_welcome():
    print('WELCOME TO PLATZI VENTAS')
    print('*' * 15)
    list_clients()
    print('*' * 15)
    print('What do you like to do today?')
    print('[C]reate client.')
    print('[R]ead a client.')
    print('[U]pdate client.')
    print('[D]elete client.')
    print('----------------')
    print('[L]ist Clients.')
    print('----------------')
    print('[E]xit.')

def _get_client_name():
    client_name = None
    while not client_name:
        client_name = input('What is the client name? :::')
        if client_name == 'exit':
            client_name = None
            #print('..........')
            sys.exit()
    return client_name


def _get_client_field(field_name):
    field = None
    while not field:
        field = input('What is the client {}? :::'.format(field_name))
        if field == 'exit':
            field = None
            sys.exit()
    return field


def _get_update_client_name():
    return input('What is the update client name? :::')
 
def run():
    _initialize_clients_from_storage()
    exit = True
    while exit:
        _print_welcome()
        command = input()
        command = command.upper()

        if command == 'C':
            #client_name = _get_client_name()
            client = {
                'name': _get_client_field('name'),
                'company': _get_client_field('company'),
                'email': _get_client_field('email'),
                'position': _get_client_field('position'),
            }
            create_client(client)
            list_clients()
        elif command == 'D':
            client_name = _get_client_name()
            delete_client(client_name)
            list_clients()
        elif command == 'R':
            #client_name = _get_client_name()
            found = read_client()
        elif command == 'U':
            client_name = _get_client_name()
            update_client_name = _get_update_client_name()
            update_client(client_name,update_client_name)
            list_clients()
        elif command == 'L':
            list_clients()
        elif command == 'E':
            _save_clients_to_storage()
            print('Data saved.')
            print('Bye')
        else:
            print('Invalid command.')
        print('Continue (y|n):::')
        cont = input()
        cont = cont.upper()
        if cont == 'Y':
            exit = True
        else:
            exit = False
            _save_clients_to_storage()
            print('Data saved.')
            print('Bye')

if __name__ == '__main__':
    run()

class Airplane:
    def __init__(self, passengers, seats, pilots=[]):
        self.passengers = passengers
        self.seats = seats
        self._pilots = pilots

    def takeoff(self):
        print('Airplane takeoff...')

if __name__ == '__main__':
    airplane_722 = Airplane(passengers=20, seats=30, pilots=['Tom','Jerry'])
    airplane_722.passengers = 25
    print('Seats: {}'.format(airplane_722.seats))
    print('Passengers: {}'.format(airplane_722.passengers))
    print('## Private Pilots: ## ## {} ##'.format(airplane_722._pilots))
    airplane_722.takeoff()

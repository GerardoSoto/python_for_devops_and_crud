PASSWORD_HASH = '1234567890'

def password_required(func):
    def wrapper():
        password = input('Input your password: ')
        if password == PASSWORD_HASH:
            return func()
        else:
            print('Password incorrect.')
    return wrapper

@password_required
def needs_password():
    print('La contraseña es correcta')


def upper(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        print('type[result:::{}]'.format(type(result)))
        print('var[result]:::{}'.format(result))
        print('var[*args]::::{}'.format(*args))
        #print('var[**kw]:::::{}'.format(**kwargs))
        return result.upper()
    return wrapper

@upper
def say_my_name(name):
    return 'Hello, {}'.format(name)

if __name__ == '__main__':
    print('{}'.format(say_my_name('Gerard')))

"""
Using of specials methods __str__ and __repr__ to show the information of object.

URL: https://www.youtube.com/watch?v=ewKUjEgg6mM&list=PLg9145ptuAigw5pV_DRznXdOsX19dorDs&index=12
"""

class Student:
    def __init__(self, name, last_name, age):
        self.name = name
        self.last_name = last_name
        self.age = age

    
    def __str__(self):
        return "__str__ Hi, I'm {} {} and I'm {} years old.".format(self.name, self.last_name, self.age)


    def __repr__(self):
        return f"__repr__ Hi, I'm {self.name} {self.last_name} and I'm {self.age} years old."


new_student = Student('Gerard', 'Soto', 26)
print("{}".format(new_student))
print(f"{new_student !r}")


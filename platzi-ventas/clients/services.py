import csv,os

from clients.models import Client

class ClientService:
    def __init__(self, table_name):
        self.table_name = table_name

    def create_client(self, client):
        with open(self.table_name, mode='a') as file_table:
            writer = csv.DictWriter(file_table, fieldnames = Client.schema())
            writer.writerow(client.to_dict())

    def list_clients(self):
        with open(self.table_name, mode='r') as file_table:
            reader = csv.DictReader(file_table, fieldnames = Client.schema())
            # reader es un objeto iterable, 
            print(reader)
            return list(reader)


    def update_client(self, updated_client):
        clients = self.list_clients()

        updated_clients = []
        for client in clients:
            if client['uid'] == updated_client.uid:
                # Cuando encuentre el ID del cliente que estoy
                #modificando, guarda el que le envio y no el que
                #estaba en la lista actual
                updated_clients.append(updated_client.to_dict())
            else:
                # Si no encuentra el ID del cliente que quiero
                # actualizar, va agregando a la lista, todos 
                # los clientes diferentes al mio sin modificar
                updated_clients.append(client)
        self._save_to_disk(updated_clients)


    def delete_client(self, client_to_delete):
        clients = self.list_clients()
        updated_clients = []
        for client in clients:
            if client['email'] == client_to_delete.email:
                pass
            else:
                updated_clients.append(client)
        self._save_to_disk(updated_clients)


    def _save_to_disk(self, clients):
        tmp_table_name = self.table_name + '.tmp'
        with open(tmp_table_name, 'w') as file_tmp_table:
            writer = csv.DictWriter(file_tmp_table, fieldnames = Client.schema())
            writer.writerows(clients)
        os.remove(self.table_name)
        os.rename(tmp_table_name, self.table_name)

        

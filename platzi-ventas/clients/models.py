import uuid

class Client:
    """ Class Client """
    def __init__(self, name, company, email, position, uid=None):
        self.name = name
        self.company = company
        self.email = email
        self.position = position
        self.uid = uid or uuid.uuid4()# Utiizado en la industria

    def to_dict(self):
        """ Method to tranformate de object in a dict """
        return vars(self)

    @staticmethod
    def schema():
        """ Method static to use without initialize the object"""
        return ['name', 'company', 'email', 'position', 'uid']

import click
from clients.services import ClientService
from clients.models import Client

@click.group() # <- Create the method CLIENTS in a decorete
def clients():
    """ Managers the clients lifecycle"""
    pass

@clients.command() # <- click.group
@click.option('-n', '--name',
            type=str,
            prompt=True,
            help='The client name')
@click.option('-c', '--company',
            type=str,
            prompt=True,
            help='The client company')
@click.option('-e', '--email',
            type=str,
            prompt=True,
            help='The client e-mail')
@click.option('-p', '--position',
            type=str,
            prompt=True,
            help='The client position')
@click.pass_context # pass context ctx to method
def create(ctx, name, company, email, position):
    """ Creates a new clients """
    client = Client(name, company, email, position)

    client_service = ClientService(ctx.obj['clients_table'])
    client_service.create_client(client)


@clients.command()
@click.pass_context
def list(ctx):
    """ List of all clients """
    client_service = ClientService(ctx.obj['clients_table'])
    clients_list = client_service.list_clients()

    click.echo(' ID  |  NAME   |  COMPANY   |  EMAIL  |  POSITION')
    click.echo('*' * 100)
    for client in clients_list:
        click.echo('{} | {} | {} | {} | {} '.format(
            client['uid'],
            client['name'],
            client['company'],
            client['email'],
            client['position']))

@clients.command()
@click.argument('client_uid',
                type=str)
@click.pass_context
def update(ctx, client_uid):
    """ Update a client """
    client_service = ClientService(ctx.obj['clients_table'])
    clients_list = client_service.list_clients()

    client = [client for client in clients_list if client['uid'] == client_uid]# client_uid = click.arg

    if client:
        client_updated = _update_client_flow(Client(**client[0]))
        client_service.update_client(client_updated)
        click.echo('Client updated')
    else:
        click.echo('Client not found.')


def _update_client_flow(client):
    click.echo('Leave empty if you don\'t want to modify the value')
    client.name = click.prompt('New name: ', type = str, default = client.name)
    client.company = click.prompt('New company: ', type = str, default = client.company)
    client.email = click.prompt('New e-mail: ', type = str, default = client.email)
    client.position = click.prompt('New position: ', type = str, default = client.position)

    return client

@clients.command()
@click.argument('client_email',
                type=str)
@click.pass_context
def search(ctx, client_email):
    """ Search a client """
    client_service = ClientService(ctx.obj['clients_table'])
    clients_list = client_service.list_clients()

    client_to_search = [client for client in clients_list if client['email'] == client_email]
    
    if client_to_search:
        click.echo('Client found.')
        print(client_to_search)

    else:
        click.echo('Client {} not found.'.format(client_email))

@clients.command()
@click.argument('client_email',
                type=str)
@click.pass_context
def delete(ctx, client_email):
    """Delete a client"""
    clients_service = ClientService(ctx.obj['clients_table'])
    clients_list = clients_service.list_clients()

    client_to_delete = [client for client in clients_list 
                        if client['email'] == client_email]
    if client_to_delete:
#        print('cliente no descomprimido:::')
#        print(client_to_delete)
#        print('cliente descomprimido:::')
#        print(**client_to_delete)
        client_to_delete = Client(**client_to_delete[0])
        clients_service.delete_client(client_to_delete)
        click.echo('Client {} deleted'.format(client_email))
    else:
        click.echo('Client {} not found'.format(client_email))



#alias para declarar todos los metodos de este archivo en un code:
all = clients

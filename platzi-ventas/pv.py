import click

# Import the commands in the file /commands/clients.py
from clients import commands as clients_commands

CLIENTS_TABLE = '.clients.csv'
@click.group()# <- punto de entrada
@click.pass_context # <- Objeto context: ctx
def cli(ctx):
    ctx.obj = {}# <- Dict
    ctx.obj['clients_table'] = CLIENTS_TABLE
    

cli.add_command(clients_commands.all)
